Aufgabe 1:

Schreibt ein Programm, dass eine NullPointerException verursacht


Aufgabe 2 (optinal):

Schreiben Sie ein Programm, dass eine beliebige Anzahl von ganzen
Zahlen als Programmparameter bekommt und diese absteigend sortiert
ausgibt.
Hinweise:
- Programmparameter sind über String[] args der main-Methode
aufrufbar.
- Zur Konvertierung von String nach Integer können Sie die Integer
.parseInt(s)-Methode verwenden.













Aufgabe 3 (optinal):

Das Programm soll drei Zahlen von der Kommandozeile nehmen, einen An-
lagebetrag, einen Zinssatz und einen Anlagedauer in Jahren und den dafür
resultierenden verzinsten Geldbetrag ausgeben.
