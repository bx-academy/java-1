Wiederholung

- main-Methode "public static void main(String[] args){}"
- minimale Klasse "class <klassenname> {}"
- Klasse kann beinhalten:
  - Attributen
    "class <klassenname> {
       <sichtbarkeit> <Datentyp> <attributsname>;
     }"
  - Methoden
    "class <klassenname> {
       <sichtbarkeit> <Return-Typ> <methoden-name> (<argumente>) {
        //statements
       }
    }"
    <argumente>: "<Datentyp1> <argument-name1>,
                <Datentyp2> <argument-name2>,
                ..."
    <Return-Typ>: "void" wenn es kein Rückgabewert gibt
    - Konstruktor: Spezielle Methode, erzeugt die Instanz der Klasse.
    "class <klassenname> {
        <Sichtbarkeit> <Klassenname>(<argumente>) {
          //statements
        }
    }"
- Objekt: Instanz einer Klasse,
  Ausprägung einer Klasse mit bestimmen Werten
- <Klassenname> <variable-name> = new <Klassenname>(<Argumente>);
- "this" in einer Objektmethode, das aktuelle Objekt
- Variabel Container für Werte eines bestimmten Typs
- Deklariere Variabel "<Datentyp> <variabel-name>;"
- Wertzuweisung "<variabel-name> = <Wert>";
- Eine Variabel ist gütig bis zum "}" des aktuellen Blocks
- Kontrollstrukturen:
   - for
   - while
   - do-while
   - if
   - if-else
   - switch
